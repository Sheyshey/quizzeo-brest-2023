import { createApp } from "./app.js"
import { router as quizzRouter } from "./routes/quizz.js"

const app = createApp()

app.use("/quizz", quizzRouter)
app.get("/test", (req, res) => {
    res.send("OK")
})

app.listen(4000, () => {
    console.log("serveur démarré sur le port 4000...");
})
