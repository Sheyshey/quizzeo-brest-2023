// import { expect } from "chai";
// import { loadCountries } from "../countries.js";
// import { createQuestion, createQuizz, shuffle, uniq } from "../quizzGenerator.js";
// import { answerQuestion, getSimilarity, isAnswerCorrect, setGuessType } from "../quizz.js";

// describe('quizzGenerator Tests', () => {

//     it('Should create 20 questions from countries data', async () => {
//         const countries = await loadCountries()
//         const quizz = createQuizz(countries)
//         expect(quizz).to.be.an("Object")
//         expect(quizz.questions).to.be.ok
//         expect(quizz.questions).to.be.an("Array")
//         expect(quizz.questions.length).to.eql(20)
//         for (const question of quizz.questions) {
//             expect(question.type.tip).to.be.ok
//             expect(question.type.tip).to.be.oneOf(["capital", "country", "flag"])
//             expect(question.type.answer).to.be.ok
//             expect(question.type.answer).to.be.oneOf(["capital", "country", "flag"])
//             expect(question.tip).to.be.ok
//             expect(question.tip).to.not.be.empty
//             expect(question.answer).to.be.ok
//             expect(question.answer).to.not.be.empty
//             expect(question.choices).to.be.ok
//             expect(question.choices).to.be.an("Array")
//             expect(question.choices.length).to.eql(4)
//             for (let i = 0; i < 4; i++) {
//                 const choice = question.choices[i]
//                 expect(choice).to.be.ok
//                 expect(choice).to.not.be.empty
//             }
//             expect(uniq(question.choices).length).to.eql(4)
//             expect(question.answer).to.be.oneOf(question.choices)
//         }
//         expect(quizz.currentQuestion).to.be.a("Number")
//         expect(quizz.currentQuestion).to.eql(0)
//         expect(quizz.questions[quizz.currentQuestion].guessType).to.be.null
//         expect(quizz.questions[quizz.currentQuestion].guess).to.be.null
//     });

//     it('Should go forward when current question is answered cash correctly', async () => {
//         const countries = await loadCountries()
//         const quizz = createQuizz(countries)
//         const question = quizz.questions[quizz.currentQuestion]
//         const guess = question.answer
//         setGuessType(quizz, "cash")
//         expect(question.choices.length).to.eql(4)
//         answerQuestion(quizz, guess)
//         expect(quizz.currentQuestion).to.eql(1)
//         expect(quizz.questions[quizz.currentQuestion - 1].guessType).to.eql("cash")
//         expect(quizz.questions[quizz.currentQuestion - 1].guess).to.eql(guess)
//     })

//     it('Should go forward when current question is answered cash incorrectly', async () => {
//         const countries = await loadCountries()
//         const quizz = createQuizz(countries)
//         const question = quizz.questions[quizz.currentQuestion]
//         const guess = "yolo"
//         setGuessType(quizz, "cash")
//         expect(question.choices.length).to.eql(4)
//         answerQuestion(quizz, guess)
//         expect(quizz.currentQuestion).to.eql(1)
//         expect(quizz.questions[quizz.currentQuestion - 1].guessType).to.eql("cash")
//         expect(quizz.questions[quizz.currentQuestion - 1].guess).to.eql(guess)
//     })

//     it('Should go forward when current question is answered "square" incorrectly', async () => {
//         const countries = await loadCountries()
//         const quizz = createQuizz(countries)
//         const question = quizz.questions[quizz.currentQuestion]
//         const badAnswers = question.choices.filter((c) => c != question.answer)
//         const guess = badAnswers[Math.floor(Math.random() * 3)]
//         setGuessType(quizz, "square")
//         expect(question.choices.length).to.eql(4)
//         answerQuestion(quizz, guess)
//         expect(quizz.currentQuestion).to.eql(1)
//         expect(quizz.questions[quizz.currentQuestion - 1].guessType).to.eql("square")
//         expect(quizz.questions[quizz.currentQuestion - 1].guess).to.eql(guess)
//     })

//     it('Should go forward when current question is answered "square" correctly', async () => {
//         const countries = await loadCountries()
//         const quizz = createQuizz(countries)
//         const question = quizz.questions[quizz.currentQuestion]
//         const guess = question.answer
//         setGuessType(quizz, "square")
//         expect(question.choices.length).to.eql(4)
//         answerQuestion(quizz, guess)
//         expect(quizz.currentQuestion).to.eql(1)
//         expect(quizz.questions[quizz.currentQuestion - 1].guessType).to.eql("square")
//         expect(quizz.questions[quizz.currentQuestion - 1].guess).to.eql(guess)
//     })

//     it('Should compare exact "cash" answer with the correct answer', async () => {
//         const countries = await loadCountries()
//         const quizz = createQuizz(countries)
//         const question = quizz.questions[quizz.currentQuestion]
//         const guess = question.answer
//         expect(getSimilarity(guess, question.answer)).to.be.above(0.8)

//         setGuessType(quizz, "cash")
//         answerQuestion(quizz, guess)
//         expect(isAnswerCorrect(question)).to.be.true
//     })

//     it('Should compare mispelled "cash" answer with the correct answer', async () => {
//         const countries = await loadCountries()
//         const quizz = createQuizz(countries)
//         const question = quizz.questions[quizz.currentQuestion]
//         const mispell = "f"
//         const guess = question.answer + mispell
//         expect(getSimilarity(guess, question.answer)).to.be.above(0.8)

//         setGuessType(quizz, "cash")
//         answerQuestion(quizz, guess)
//         expect(isAnswerCorrect(question)).to.be.true
//     })

//     it('Should compare "cash" answer with an incorrect answer', async () => {
//         let countries = await loadCountries()
//         countries = shuffle(countries)
//         const quizz = createQuizz(countries)
//         const question = quizz.questions[quizz.currentQuestion]
//         const badAnswers = question.choices.filter((c) => c != question.answer)
//         const guess = badAnswers[Math.floor(Math.random() * 3)]
//         console.log("choices", question.choices, "bad:", badAnswers, "ans", question.answer);
//         console.log("guess:", guess, "ans", question.answer);
//         const similarity = getSimilarity(guess, question.answer)
//         console.log("sim:", similarity);
//         expect(similarity).to.be.below(0.8)

//         setGuessType(quizz, "cash")
//         answerQuestion(quizz, guess)
//         expect(isAnswerCorrect(question)).to.be.false
//     })

//     it('Should create one question from 4 countries data', async () => {
//         const countries = await loadCountries()
//         const question = createQuestion(countries.slice(0, 4))
//         expect(question.type.tip).to.be.ok
//         expect(question.type.tip).to.be.oneOf(["capital", "country", "flag"])
//         expect(question.type.answer).to.be.ok
//         expect(question.type.answer).to.be.oneOf(["capital", "country", "flag"])
//         expect(question.tip).to.be.ok
//         expect(question.tip).to.not.be.empty
//         expect(question.answer).to.be.ok
//         expect(question.answer).to.not.be.empty
//         expect(question.choices).to.be.ok
//         expect(question.choices).to.be.an("Array")
//         expect(question.choices.length).to.eql(4)
//         for (let i = 0; i < 4; i++) {
//             const choice = question.choices[i]
//             expect(choice).to.be.ok
//             expect(choice).to.not.be.empty
//         }
//         expect(question.answer).to.be.oneOf(question.choices)
//     });
// });
