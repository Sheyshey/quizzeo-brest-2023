export const createChoices = (countries) => {
  const choices = []
  //TODO
  return choices
}

export const createQuizz = (countries) => {
  const quizz = { questions: [], currentQuestion:0 }
  //TODO
  return quizz
}

export const createQuestion = (choices) => {
  const types = [
      { tip: "flag", answer: "country" },
      { tip: "country", answer: "flag" },
      { tip: "capital", answer: "country" },
      { tip: "country", answer: "capital" }
  ]
  const type = types[Math.floor(Math.random()*4)]
  const question = {
      type: type,
      choices: choices.map((c) => c[type.answer])
  }
  //TODO
  return question
}

export function shuffle([...a]) {
  let j, x, i;
  for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
  }
  return a;
}

export function uniq(arr) {
  return arr.filter((item, i, ar) => ar.indexOf(item) === i);
}
