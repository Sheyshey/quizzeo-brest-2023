import { Router } from "express"
import { readFile } from 'node:fs/promises'

const router = Router()
router.post("/start", async (req, res) => {
    let data = await readFile('countries.json')
    data = JSON.parse(data)
    res.json(data)
})

router.get("/question/:id", (req, res) => {
    res.render("newQuestion", { desiredInfo: "la capitale", tipType: "country", tip: "France", id: req.params.id })
})

router.post("/chooseAnswerType/:id", (req, res) => {

    res.send(req.body.answerType)
})

router.get("/dashboard/:name", (req, res) => {
    res.render("index", { name: req.params.name })
})

export { router }
